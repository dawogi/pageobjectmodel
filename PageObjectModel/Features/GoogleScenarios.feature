﻿Feature: Google Scenarios
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Background: 
Given Navigate to the Google

@Chrome
Scenario: 12. Select Just Show Text Radio button
	When Click Google Settings
	And Go to the preference site
	When Select the Just Show Text radio button
	#Then Angola button is selected

@Chrome
Scenario: 13. Select Just Show Text Radio button
	When Click Google Settings
	And Go to the preference site
	When Go to edit languages
	And Select below languages
	#Then I see
	#| expectedText |
	#|	English    |

@Chrome
Scenario: 14. Using dynamic set
	When Click Google Settings
	And Navigate to advanced search
	And Search for pages with this parameters
	| words                  | exactWords    | anyWords              | noWords                 | from | to        |
	| tri-colour rat terrier | "rat terrier" | miniature OR standard | -rodent -"Jack Russell" | "100"  | 40..50 kg |

@Chrome
Scenario: 14. Using dynamic instance
	When Click Google Settings
	And Navigate to advanced search
	And Search for pages using dynamic instance
	| words                  | exactWords    | anyWords              | noWords                 | from | to        |
	| tri-colour rat terrier | "rat terrier" | miniature OR standard | -rodent -"Jack Russell" | "100"  | 40..50 kg |
