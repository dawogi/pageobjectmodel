﻿Feature: WikipediaScenarios
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Background: 
Given Navigate to the homepage

@Chrome
Scenario: 05. Return click
	When Click the English link
	Then "Welcome to" is in the PageSoruce

@Chrome
Scenario: 06. Void click
	When Open the language list
	Then Language list is displayed
	Then I see
	| expectedText |
	| Esperanto    |

@Chrome
Scenario: 07. POM Navigation
	When Click the English link
	And Go to All Portal
	And Navigate to Comunity page
	When Go to Dashboard
	Then "Welcome to the Wikipedia Dashboard!" is in the PageSoruce

@Chrome
Scenario: 08. Using SendKeys
	When Search for "Selenium"
	Then Page Url contains "/Selenium"

@Chrome
Scenario: 09. Select by Value
	When Select the languagee with the value "de"
	When Search for "Selenium"
	Then Page Url contains "de.wikipedia.org/wiki/Selenium"

@Chrome
Scenario: 10. Select by Text
	When Select the language with the text "Deutsch"
	When Search for "Selenium"
	Then Page Url contains "de.wikipedia.org/wiki/Selenium"

@Chrome
Scenario: 11. Select by Index
	When Select the language with the index 8
	When Search for "Selenium"
	Then Page Url contains "de.wikipedia.org/wiki/Selenium"