﻿Feature: Base Scenarios
	These scenarios can be used in any project

Background: 
Given Navigate to the homepage

@Chrome
Scenario: 01. Validate the title of a website
	Then Page title contains "Wikipedia"

@Chrome
Scenario: 02. Validate the Url of a website
	Then Page Url contains "www.wikipedia.org"

@Chrome
Scenario: 03. Validate the PageSource string on the website
	Then "The Free Encyclopedia" is in the PageSoruce

@Chrome
Scenario: 04. Validate sxistance of multiply texts in PageSource
	Then I see
	| expectedText |
	| English      |
	| Deutsch      |

