﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using PageObjectModel.Util.Selenium;
using System;

namespace PageObjectModel.Util.Extensions
{
    public static class WebElementExtensions
    {
        public static bool WeElementIsEnabled(this IWebElement element, int sec = 10)
        {
            var wait = new WebDriverWait(Driver.Browser(), TimeSpan.FromSeconds(sec));
            return wait.Until(d =>
            {
                try
                {
                    element.WeHighlightElement();
                    return element.Enabled;
                }
                catch (StaleElementReferenceException)
                {
                    return false;
                }
            }
            );
        }

        public static void WeSelectDropdownByIndex(this IWebElement element, int index, int sec = 10)
        {
            element.WeElementIsEnabled(sec);
            var selectElement = new SelectElement(element);
            selectElement.SelectByIndex(index);
        }

        public static void WeSelectDropdownByText(this IWebElement element, string text, int sec = 10)
        {
            element.WeElementIsEnabled(sec);
            var selectElement = new SelectElement(element);
            selectElement.SelectByText(text);
        }

        public static void WeSelectDropdownByValue(this IWebElement element, string value, int sec = 10)
        {
            element.WeElementIsEnabled(sec);
            var selectElement = new SelectElement(element);
            selectElement.SelectByValue(value);
        }

        public static string WeGetAttributeValue(this IWebElement element)
        {
            return element.GetAttribute("value");
        }

        public static void WeSendKeys(this IWebElement element, string text, int sec = 10, bool clearFirst = false)
        {
            element.WeElementIsDisplayed(sec);
            if (clearFirst) element.Click();
            element.SendKeys(text);

        }

        public static void WeElementToBeClickable(this IWebElement element, int sec = 10)
        {
            var wait = new WebDriverWait(Driver.Browser(), TimeSpan.FromSeconds(sec));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void WeClick(this IWebElement element, int sec = 10)
        {
            element.WeElementToBeClickable(sec);
            element.WeHighlightElement();
            element.Click();
        }

        public static void WeHighlightElement(this IWebElement element)
        {
            var js = (IJavaScriptExecutor)Driver.Browser();
            js.ExecuteScript("arguments[0].style.border = '4px solid blue'", element);
        }

        public static bool WeElementIsDisplayed(this IWebElement element, int sec = 10)
        {
            var wait = new WebDriverWait(Driver.Browser(), TimeSpan.FromSeconds(sec));
            return wait.Until(d =>
            {
                try
                {
                    element.WeHighlightElement();
                    return element.Displayed;
                }
                catch (ElementNotVisibleException)
                {
                    return false;
                }
            }
            );
        }
    }
}
