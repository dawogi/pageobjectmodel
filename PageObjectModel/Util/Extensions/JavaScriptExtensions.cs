﻿using OpenQA.Selenium;
using PageObjectModel.Util.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.Util.Extensions
{
    public static class JavaScriptExtensions
    {
        public static void JsSendKeys(this string element, string value)
        {
            var js = (IJavaScriptExecutor) Driver.Browser();
            js.ExecuteScript("document.getElemenyById('" + element + "').value = '" + value + "'", element);
        }

        public static void JsSendIntKeys(this string element, int value)
        {
            var js = (IJavaScriptExecutor)Driver.Browser();
            js.ExecuteScript("document.getElemenyById('" + element + "').value = '" + value + "'", element);
        }
    }
}
