﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using PageObjectModel.Util.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.Util.Extensions
{
    public static class WebDriverExtensions
    {
        public static string WdGetFieldVAlue(this By locator, int sec = 10)
        {
            return locator.WdFindElement(sec).GetAttribute("value");
        }

        public static void WdSendKeys(this By locator, string text, int sec = 10)
        {
            locator.WdFindElement(sec).SendKeys(text);
        }

        public static void WdClickByIndex(this By locator, int index = 0, int sec = 10)
        {
            var driver = Driver.Browser();
            var myLocator = driver.FindElements(locator);
            myLocator[index].WeClick();
        }

        public static object WdHighlight(this By locator)
        {
            var driver = Driver.Browser();
            var myLocator = driver.FindElement(locator);
            var js = (IJavaScriptExecutor)driver;
            return js.ExecuteScript("arguments[0].style.border = '4px solid red'", myLocator);
        }

        public static void WdClick(this By locator, int sec = 10)
        {
            locator.WdFindElement(sec).Click();
        }

        public static IWebElement WdFindElement(this By locator, int sec = 10)
        {
            var wait = new WebDriverWait(Driver.Browser(), TimeSpan.FromSeconds(sec));
            return wait.Until(drv =>
                {
                    try
                    {
                        locator.WdHighlight();
                        return drv.FindElement(locator);
                    }
                    catch (NoSuchElementException)
                    {
                        return null;
                    }
                }
            );
        }


    }
}
