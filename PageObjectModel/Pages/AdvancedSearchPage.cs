﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectModel.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PageObjectModel.Pages
{
    public class AdvancedSearchPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "xX4UFf")]
        public IWebElement TxtWords { get; set; }

        [FindsBy(How = How.Id, Using = "CwYCWc")]
        public IWebElement TxtExactWords { get; set; }

        [FindsBy(How = How.Id, Using = "mSoczb")]
        public IWebElement TxtAnyWords { get; set; }

        [FindsBy(How = How.Id, Using = "t2dX1c")]
        public IWebElement TxtnoWords { get; set; }

        [FindsBy(How = How.Id, Using = "LK5akc")]
        public IWebElement TxtFrom { get; set; }

        public void FindPagesWithDynamicInstance(Table table)
        {
            dynamic item = table.CreateDynamicInstance();

            TxtWords.WeSendKeys((string)item.words);
            TxtExactWords.WeSendKeys((string)item.exactWords);
            TxtAnyWords.WeSendKeys((string)item.anyWords);
            TxtnoWords.WeSendKeys((string)item.noWords);
            TxtFrom.WeSendKeys((string)item.from);
        }

        public void FindPagesWithDynamicSet(Table table)
        {
            var details = table.CreateDynamicSet();
            foreach (var item in details)
            {
                TxtWords.WeSendKeys((string)item.words);
                TxtExactWords.WeSendKeys((string)item.exactWords);
                TxtAnyWords.WeSendKeys((string)item.anyWords);
                TxtnoWords.WeSendKeys((string)item.noWords);
                TxtFrom.WeSendKeys((string)item.from);
            }
        }


    }
}
