﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectModel.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Pages
{
    public class EnglishHomePage : BasePage
    {
        [FindsBy(How = How.LinkText, Using = "All portals")]
        public IWebElement LinkAllPortal { get; set; }
        

        internal PortalPage NavToAllPortal()
        {

            LinkAllPortal.WeClick();
            Console.WriteLine(":: I have clicked the All Portal link");
            return InstanceOf<PortalPage>();

        }
    }
}
