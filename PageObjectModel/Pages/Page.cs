﻿using OpenQA.Selenium.Support.PageObjects;
using PageObjectModel.Util.Selenium;
using System;
using static OpenQA.Selenium.Support.PageObjects.PageFactory;

namespace PageObjectModel.Pages
{
    public abstract class Page
    {
        protected T InstanceOf<T>() where T : BasePage, new()
        {
            var pageClass = new T { Driver = Driver.Browser() };
            InitElements(Driver.Browser(), pageClass);
            var pageType = pageClass.GetType();
            Console.WriteLine("::The type of the page is " + pageType);
            return pageClass;
        }
    }
}
