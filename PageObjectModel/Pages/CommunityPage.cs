﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectModel.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.Pages
{
    public class CommunityPage : BasePage
    {
        [FindsBy(How = How.LinkText, Using = "Wikipedia:Dashboard")]
        public IWebElement LinkDashboard { get; set; }

        public DashboardPage NavToDashboard()
        {
            LinkDashboard.WeClick();
            Console.WriteLine(":: I have clicked the Dashboard link ");
            return InstanceOf<DashboardPage>();
        }

    }
}
