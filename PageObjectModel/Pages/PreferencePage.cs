﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectModel.Util.Extensions;
using System;

namespace PageObjectModel.Pages
{
    public class PreferencePage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//*[@id='pson-radio']/div[2]/span")]
        public IWebElement SelTextRadioButoon { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#langSecLink > a")]
        public IWebElement LnkLanguages { get; set; }

        [FindsBy(How = How.Id, Using = "lredit")]
        public IWebElement LnkEditLanguages { get; set; }

        public static By SelLanguage = By.CssSelector("span.jfk-checkbox.goog-inline-block.jfk-checkbox-unchecked");

        public void SelectBelowLanguages()
        {
            SelLanguage.WdClickByIndex(44);
            SelLanguage.WdClickByIndex(22);
            SelLanguage.WdClickByIndex(11);
            SelLanguage.WdClickByIndex(10);
        }

        public void SelectTheJustShowTextRadioButton()
        {
            SelTextRadioButoon.WeClick();
            Console.WriteLine("I have clicked the Show Text Radio Button");
        }


        public void ClickEditLanguageLink()
        {
            LnkLanguages.WeClick();
            Console.WriteLine(":: I have clicked the language link");
            LnkEditLanguages.WeClick();
            Console.WriteLine(":: I have clicked the edit language link");
        }

        public void AngolaButtonIsSelected()
        {

        }
    }
}
