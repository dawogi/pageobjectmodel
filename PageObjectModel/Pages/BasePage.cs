﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using PageObjectModel.Util.Extensions;
using System;
using TechTalk.SpecFlow;
using static PageObjectModel.Util.Selenium.Driver;
using static PageObjectModel.Util.Selenium.Settings;

namespace PageObjectModel.Pages
{
    public class BasePage : Page
    {
        public IWebDriver Driver { get; internal set; }
        public string GetTitle => Driver.Title;
        public string GetUrl => Driver.Url;
        public string GetPageSource => Driver.PageSource;

        [FindsBy(How = How.Id, Using = "js-link-box-en")]
        public IWebElement LinkEnglish { get; set; }

        [FindsBy(How = How.Id, Using = "js-lang-list-button")]
        public IWebElement LinkLangList { get; set; }

        [FindsBy(How = How.ClassName, Using = "lang-list-active")]
        public IWebElement ValLanguageList { get; set; }

        [FindsBy(How = How.Id, Using = "n-portal")]
        public IWebElement LinkCommunity { get; set; }

        [FindsBy(How = How.Name, Using = "search")]
        public IWebElement TxtSearchArticle { get; set; }

        [FindsBy(How = How.ClassName, Using = "pure-button-primary-progressive")]
        public IWebElement BtnSearch { get; set; }

        [FindsBy(How = How.Id, Using = "searchLanguage")]
        public IWebElement SelLanguage { get; set; }

        [FindsBy(How = How.Id, Using = "fsettl")]
        public IWebElement LinkSettings { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='fsett']/a[1]")]
        public IWebElement LinkSearchSettings { get; set; }

        public static By LinkAdvancedSearch = By.CssSelector("#fsett > a:nth-child(2)");


        public AdvancedSearchPage ClickToAdvancedSearch()
        {
            LinkAdvancedSearch.WdClick();
            Console.WriteLine(":: I have clicked the Advanced Search");
            return InstanceOf<AdvancedSearchPage>();
        }

        public static By SelFindLanguage = By.Id("searchLanguage");

        public PreferencePage GoToPreferences()
        {
            LinkSearchSettings.WeClick();
            Console.WriteLine(":: I have clicked the Search Settings");
            return InstanceOf<PreferencePage>();
        }

        public void ClickGoogleSettings()
        {
            LinkSettings.WeClick();
            Console.WriteLine(":: I have clicked the Settingslink");
        }

        public void SelectLanguageByIndex(int index)
        {
            SelLanguage.WeSelectDropdownByIndex(index);
            Console.WriteLine(":: I have selected index '8'");
        }

        public void SelectLanguageByText(string language)
        {
            SelLanguage.WeSelectDropdownByText(language);
            Console.WriteLine(":: I have selected 'German'");
        }

        public void SelectLanguageByValue(string language)
        {
            SelLanguage.WeSelectDropdownByValue(language);
            Console.WriteLine(":: I have selected 'de' for German");
        }

        public WikiPage SearchArticle(string article)
        {
            TxtSearchArticle.WeSendKeys(article);
            Console.WriteLine(":: The search field has the value: " + TxtSearchArticle.WeGetAttributeValue());
            BtnSearch.WeClick();
            Console.WriteLine(":: I have search for " + article);
            return InstanceOf<WikiPage>();
        }

        public CommunityPage NavToComunityPage()
        {
            LinkCommunity.WeClick();
            Console.WriteLine(":: I have clicked the Community link");
            return InstanceOf<CommunityPage>();
        }

        public void ThenLanguageListIsDisplayed()
        {
            ValLanguageList.WeElementIsDisplayed();
        }

        public void OpenLanguageList()
        {
            Console.WriteLine(":: I have opened the language list");
            LinkLangList.WeClick();
        }

        public EnglishHomePage NavEnglishHomePage()
        {
            LinkEnglish.WeClick();
            Console.WriteLine(":: I have clicked the LinkEnglish link");
            return InstanceOf<EnglishHomePage>();
        }

        public void ValidateTextInPageSource(string expectedText)
        {
            var textToValidate = GetPageSource.Contains(expectedText);
            Assert.IsTrue(textToValidate, ":: this is not the expected text");
            Console.WriteLine(":: the text {0} is in the PageSource " + expectedText);
        }

        public void ValidatePageTitle(string expectedTitle)
        {
            var titleToValidate = GetTitle.Contains(expectedTitle);
            Assert.IsTrue(titleToValidate, ":: this is not the expected title");
            Console.WriteLine(":: the title of the site is " + GetTitle);
        }

        public void NavigateMainEnterPoint()
        {
            var baseUrl = BaseUrl;
            Browser().Navigate().GoToUrl(baseUrl);
            Browser().Manage().Window.Maximize();
            Console.WriteLine("::Welcome to the Wikipedia");
        }

        public void NavigateMainEnterPoint2()
        {
            var baseUrl = BaseUrl2;
            Browser().Navigate().GoToUrl(baseUrl);
            Browser().Manage().Window.Maximize();
            Console.WriteLine("::Welcome to the Wikipedia");
        }

        public void ValidatePage(string expectedUrl)
        {
            var urlToValidate = GetUrl.Contains(expectedUrl);
            Assert.IsTrue(urlToValidate, ":: this is not the expected Url");
            Console.WriteLine(":: the Url of the site is " + GetUrl);
        }

        public void ValidateMultipleTextInPageSource(Table table)
        {
            foreach (var row in table.Rows)
            {
                var textToValidate = row["expectedText"];
                Assert.IsTrue(GetPageSource.Contains(textToValidate), textToValidate + " is not the PageSource!");
                Console.WriteLine(":: The text {0} is in the PageSource ", textToValidate);
            }
        }

    }
}
