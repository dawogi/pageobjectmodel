﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class NonDynamicTableSteps : BaseSteps
    {
        [Then(@"I see")]
        public void ThenISee(Table table)
        {
            InstanceOf<BasePage>().ValidateMultipleTextInPageSource(table);
        }
    }
}
