﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class SendKeysSteps : BaseSteps
    {
        [When(@"Search for ""(.*)""")]
        public void WhenSearchFor(string article)
        {
            InstanceOf<BasePage>().SearchArticle(article);
        }
    }
}
