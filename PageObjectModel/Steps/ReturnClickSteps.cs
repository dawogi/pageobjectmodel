﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class ReturnClickSteps : BaseSteps
    {
        [When(@"Click the English link")]
        public void WhenClickTheEnglishLink()
        {
            InstanceOf<BasePage>().NavEnglishHomePage();
        }

        [When(@"Go to All Portal")]
        public void WhenGoToAllPortal()
        {
            InstanceOf<EnglishHomePage>().NavToAllPortal();
        }

        [When(@"Navigate to Comunity page")]
        public void WhenNavigateToComunityPage()
        {
            InstanceOf<BasePage>().NavToComunityPage();
        }

        [When(@"Go to Dashboard")]
        public void WhenGoToDashboard()
        {
            InstanceOf<CommunityPage>().NavToDashboard();
        }

        [When(@"Go to the preference site")]
        public void WhenGoToThePreferenceSite()
        {
            InstanceOf<BasePage>().GoToPreferences();
        }

        [When(@"Navigate to advanced search")]
        public void WhenNavigateToAdvancedSearch()
        {
            InstanceOf<BasePage>().ClickToAdvancedSearch();
        }

    }
}
