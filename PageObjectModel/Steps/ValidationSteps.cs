﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class ValidationSteps : BaseSteps
    {
        [Then(@"Language list is displayed")]
        public void ThenLanguageListIsDisplayed()
        {
            InstanceOf<BasePage>().ThenLanguageListIsDisplayed();
        }


        [Then(@"Page title contains ""(.*)""")]
        public void ThenPageTitleContains(string expectedTitle)
        {
            InstanceOf<BasePage>().ValidatePageTitle(expectedTitle);
        }

        [Then(@"Page Url contains ""(.*)""")]
        public void ThenPageUrlContains(string expectedUrl)
        {
            InstanceOf<BasePage>().ValidatePage(expectedUrl);
        }

        [Then(@"""(.*)"" is in the PageSoruce")]
        public void ThenIsInThePageSoruce(string expectedText)
        {
            InstanceOf<BasePage>().ValidateTextInPageSource(expectedText);
        }

        [Then(@"Angola button is selected")]
        public void ThenAngolaButtonIsSelected()
        {
            InstanceOf<PreferencePage>().AngolaButtonIsSelected();
        }

    }
}
