﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class DynamicTableSteps : BaseSteps
    {
        [When(@"Search for pages with this parameters")]
        public void WhenSearchForPagesWithThisParameters(Table table)
        {
            InstanceOf<AdvancedSearchPage>().FindPagesWithDynamicSet(table);
        }

        [When(@"Search for pages using dynamic instance")]
        public void WhenSearchForPagesUsingDynamicInstance(Table table)
        {
            InstanceOf<AdvancedSearchPage>().FindPagesWithDynamicInstance(table);
        }
    }
}
