﻿using PageObjectModel.Pages;
using PageObjectModel.Util.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class BaseScenariosSteps : BaseSteps
    {
        [Given(@"Navigate to the homepage")]
        public void GivenNavigateToTheHomepage() => InstanceOf<BasePage>().NavigateMainEnterPoint();

        [Given(@"Navigate to the Google")]
        public void GivenNavigateToTheGoogle()
        {
            InstanceOf<BasePage>().NavigateMainEnterPoint2();
        }

    }
}
