﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class SelectSteps : BaseSteps
    {
        [When(@"Select the languagee with the value ""(.*)""")]
        public void WhenSelectTheLanguageeWithTheValue(string language)
        {
            InstanceOf<BasePage>().SelectLanguageByValue(language);
        }

        [When(@"Select the language with the text ""(.*)""")]
        public void WhenSelectTheLanguageWithTheText(string language)
        {
            InstanceOf<BasePage>().SelectLanguageByText(language);
        }

        [When(@"Select the language with the index (.*)")]
        public void WhenSelectTheLanguageWithTheIndex(int index)
        {
            InstanceOf<BasePage>().SelectLanguageByIndex(index);
        }

        [When(@"Select the Just Show Text radio button")]
        public void WhenSelectTheJustShowTextRadioButton()
        {
            InstanceOf<PreferencePage>().SelectTheJustShowTextRadioButton();
        }

        [When(@"Select below languages")]
        public void WhenSelectBelowLanguages()
        {
            InstanceOf<PreferencePage>().SelectBelowLanguages();
        }


    }
}
