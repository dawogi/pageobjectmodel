﻿using PageObjectModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PageObjectModel.Steps
{
    [Binding]
    public sealed class VoidClickSteps : BaseSteps
    {
        [When(@"Open the language list")]
        public void WhenOpenTheLanguageList()
        {
            InstanceOf<BasePage>().OpenLanguageList();
        }

        [When(@"Click Google Settings")]
        public void WhenClickGoogleSettings()
        {
            InstanceOf<BasePage>().ClickGoogleSettings();
        }

        [When(@"Go to edit languages")]
        public void WhenGoToEditLanguages()
        {
            InstanceOf<PreferencePage>().ClickEditLanguageLink();
        }


    }
}
